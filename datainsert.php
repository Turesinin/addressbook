<?php

include "dbconnect.php";
?>

<style>

body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box}

/* Full-width input fields */
input[type=text], input[type=password] {
    width: 100%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}



hr {
    border: 1px solid #f1f1f1;
    margin-bottom: 25px;
}

/* Set a style for all buttons */
button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

button:hover {
    opacity:1;
}

/* Extra styles for the cancel button */
.cancelbtn {
    padding: 14px 20px;
    background-color: #f44336;
}

/* Float cancel and signup buttons and add an equal width */
.cancelbtn, .signupbtn {
  float: left;
  width: 50%;
}

/* Add padding to container elements */
.container {
    padding: 16px;
}

/* Clear floats */
.clearfix::after {
    content: "";
    clear: both;
    display: table;
}

    

.card {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
    margin-left: 30%;
    margin-top: 2%;
    background-color: #ffffff;
}

/* Change color on hover */


/* Change styles for cancel button and signup button on extra small screens */
@media screen and (max-width: 300px) {
    .cancelbtn, .signupbtn {
       width: 100%;
    }
}
body{
    background-color: #e79329;
}
</style>




<?php


  //Inserts data into contact table

    
if (isset($_POST['submit'])) {

    $email = $_POST['email'];
    $firstname = $_POST['first_name'];
    $lastname = $_POST['last_name'];
    $birthdate = $_POST['date_of_birth'];
    $address = $_POST['address'];
    $phone = $_POST['phone'];
 
   
    // SQL code for data insertion

    $insert = "INSERT INTO contact (EMAIL, FIRST_NAME, LAST_NAME, DATE_OF_BIRTH,ADDRESS,PHONE) VALUES ('$email','$firstname','$lastname','$birthdate','$address', '$phone')"; 
    
   $result = mysqli_query($connection, $insert) or die(mysqli_error());

    echo "<h1>Data Inserted </h1>"; //Gives an echo when data is inserted

   }

    else

        echo "<h1>Data was not added </h1>"; //gives an output when data is not inserted

    ?>

<!DOCTYPE>
<!DOCTYPE html>
<html>
<head>
      
</head>
<body class="fadeIn animated">
<div class="card" data-animate-effect="fadeIn">


<form class="slideInUp animated" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" style="border:1px solid #ccc" >
  <div class="container">
    <h1>Insert Data</h1>
    <p>Please insert correct data.</p>
    <hr>



    <label for="name"><b>Email Address</b></label>
    <input type="text" placeholder="Email Address" name="email" required>

    <label for="name"><b>First Name</b></label>
    <input type="text" placeholder="First Name" name="first_name" required>

    <label for="name"><b>Last Name</b></label>
    <input type="text" placeholder="Last Name" name="last_name" required>


    <label for="name"><b> Date of Birth</b></label>
    <input type="text" placeholder="mm/dd/yy" name="date_of_birth" required>

    <label for="name"><b>Address</b></label>
    <input type="text" placeholder="address" name="address" required>


    <label for="name"><b> Phone</b></label>
    <input type="text" placeholder="phone" name="phone" required>


    
    <p><a href="index.php" style="color:dodgerblue">See Contact Details</a></p>

    <div class="clearfix">
      <button type="submit" class="signupbtn" value="Submit" name="submit">SUBMIT</button>
    </div>
  </div>
</form>
</div>
</body>
</html>


