<?php

include "dbconnect.php";

?>


<!DOCTYPE html>
<html>
<head>


<style>
table {

  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

.container {
    padding: 5%;
}


  
.card {
    /* Effects in Card*/
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 70%;
    margin-left: 15%;
    margin-top: 2%;
    background-color: #ffffff;
}

</style>



</head>


<body>
<div class="card" data-animate-effect="fadeIn">


  <div class="container">
    
     <h1>Contact Details</h1>
	
	<table>

  <tr>
    <th>Email</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Date of Birth</th>
    <th>Address</th>
    <th>Phone Number</th>
  </tr>


 
   <?php 

   /*

  This php portion shows the data from the contact table.
  EMAIL, FIRST_NAME,LAST_NAME, DATE_OF_BIRTH, ADDRESS, PHONE are the entity of the table,

     */
         

            $showdata =  "SELECT EMAIL, FIRST_NAME,LAST_NAME, DATE_OF_BIRTH, ADDRESS, PHONE from contact";

            $result = $connection -> query($showdata);

            while($row = $result -> fetch_assoc())
            { 
               // echo "Posted By: ";
                //echo "<td>".$row['user_name']."</td>";
                echo "<tr><td>".$row['EMAIL']."</td><td>".$row["FIRST_NAME"]."</td><td>".$row['LAST_NAME']."</td><td>".$row['DATE_OF_BIRTH']."</td><td>".$row["ADDRESS"]."</td><td>".$row['PHONE']."</td></tr>";

                echo "<br>";
            }


         ?> 

         </table>  


    </div>
  </div>

</div>


</body>
</html>